var tape = require("tape"),
    immutable = require("immutable"),
    State = require("..");

tape("setStateFor(partialState)", function(assert) {
    var state = new State(),
        store = state.createStore("store", { key: null });

    assert.deepEqual(state.stateFor("store").toJS(), { key: null });
    assert.deepEqual(store.toJS(), { key: null });

    state.setStateFor("store", {
        key: "value"
    });

    assert.deepEqual(state.stateFor("store").toJS(), { key: "value" });
    assert.deepEqual(state.toJSON(), { store: { key: "value" } });

    assert.end();
});

tape("setState(partialState)", function(assert) {
    var state = new State(),
        store = state.createStore("store", immutable.Map({ key: null }));

    store.setState({
        key: "value"
    });
    assert.deepEqual(state.stateFor("store").toJS(), { key: "value" });

    try {
        store.setState();
    } catch (e) {
        assert.equal(e.message, "undefined or null passed to store store");
    }
    assert.deepEqual(state.stateFor("store").toJS(), { key: "value" });

    assert.end();
});

tape("replaceStateFor(state)", function(assert) {
    var state = new State();

    state.createStore("store", { oldKey: null });

    state.replaceStateFor(
        "store",
        immutable.fromJS({
            key: "value"
        })
    );

    assert.deepEqual(state.stateFor("store").toJS(), { key: "value" });
    assert.end();
});

tape("replaceState(state)", function(assert) {
    var state = new State(),
        store = state.createStore("store", { key: null });

    store.replaceState({
        key: "value"
    });
    assert.deepEqual(state.stateFor("store").toJS(), { key: "value" });

    store.replaceState({});
    assert.deepEqual(state.stateFor("store").toJS(), {});

    assert.end();
});

tape("updateStateFor(updateFn)", function(assert) {
    var state = new State();

    state.createStore("store", { key: null });

    state.updateStateFor("store", function(state) {
        return state.set("key", "value");
    });

    assert.deepEqual(state.stateFor("store").toJS(), { key: "value" });
    assert.end();
});

tape("updateState(updateFn)", function(assert) {
    var state = new State(),
        store = state.createStore("store", { key: null });

    store.updateState(function() {
        return { key: "value" };
    });
    assert.deepEqual(state.stateFor("store").toJS(), { key: "value" });

    assert.end();
});

tape("events", function(assert) {
    var state = new State(),
        addStoreCalled = 0,
        removeStoreCalled = 0,
        updateCalled = 0,
        storeUpdateCalled = 0,
        store;

    state.on("addStore", function() {
        addStoreCalled += 1;
    });
    state.on("removeStore", function() {
        removeStoreCalled += 1;
    });
    state.on("update", function() {
        updateCalled += 1;
    });

    store = state.createStore("store", { key: null });

    store.on("update", function() {
        storeUpdateCalled += 1;
    });
    store.setState({ key: "value" });

    assert.deepEqual(state.toJS(), { store: { key: "value" } });
    state.removeStore("store");
    assert.deepEqual(state.toJS(), {});

    assert.equal(addStoreCalled, 1);
    assert.equal(removeStoreCalled, 1);
    assert.equal(updateCalled, 2);
    assert.equal(storeUpdateCalled, 1);

    assert.end();
});

tape("todos store test", function(assert) {
    var state = new State();

    var ID = 0;

    var todos = state.createStore("todos", {
        list: immutable.List()
    });

    todos.create = function(text) {
        var id = ID++;

        todos.updateState(function(prev) {
            return prev.update("list", function(list) {
                return list.push(
                    immutable.Map({
                        id: id,
                        text: text
                    })
                );
            });
        });
    };

    todos.remove = function(id) {
        todos.updateState(function(prev) {
            return prev.update("list", function(list) {
                return list.remove(
                    list.findIndex(function(todo) {
                        return todo.get("id") === id;
                    })
                );
            });
        });
    };

    todos.create("Hello, world!");
    assert.deepEqual(todos.toJS(), {
        list: [{ id: 0, text: "Hello, world!" }]
    });

    var storeUnsafeSetState = false;
    var stateUnsafeSetState = false;

    todos.on("unsafeSetState", function(s) {
        storeUnsafeSetState = true;
        assert.deepEqual(s.toJS(), {
            list: [{ id: 0, text: "Hello, world!" }]
        });
    });
    state.on("unsafeSetState", function(s) {
        stateUnsafeSetState = true;
        assert.deepEqual(s.toJS(), {
            todos: { list: [{ id: 0, text: "Hello, world!" }] }
        });
    });

    state.unsafeSetState(state.state());
    assert.deepEqual(todos.toJS(), {
        list: [{ id: 0, text: "Hello, world!" }]
    });

    state.unsafeSetStateFor(todos.name(), todos.state());
    assert.deepEqual(todos.toJS(), {
        list: [{ id: 0, text: "Hello, world!" }]
    });

    todos.remove(0);
    assert.deepEqual(todos.toJS(), { list: [] });

    assert.equal(storeUnsafeSetState, true);
    assert.equal(stateUnsafeSetState, true);

    state.forceUpdateAll();

    assert.end();
});
