# js-state-immutable

immutable state management for applications

## States

a State is the single source of truth for application,
states have stores which are used to update the state.

```javascript
createStore(name: String[, initialState: Object | Immutable.Map]) -> Store
removeStore(name: String)
```

### Example

```javascript
import State from "@nathanfaucett/state-immutable";

const state = new State();
const store = state.createStore("storeName", {
    key: "value"
});

state.on("update", name => {
    console.log("Store " + name + " updated!");
    console.log(state.current());
});

store.setState({
    new_key: "value"
});
```

## Stores

stores are views into your state, they trigger updates with these methods

```javascript
setState(partialState: Object | Immutable.Map)
updateState(updateFn: Function(prevState: Object | Immutable.Map) -> Immutable.Map)
replaceState(nextState: Object | Immutable.Map)
```

### Example

```javascript
import { Map, fromJS } from "immutable";
import state from "./state";

const store = state.createStore("storeName", {
    key: "value"
});

console.log(store.current()); // Map { key: "value", new_key: "value" }

store.replaceState({
    replaced: true
});
console.log(store.current()); // Map { replaced: true }

store.replaceState({
    replaced: true
});
console.log(store.current()); // Map { replaced: true }
```

### Example Store

```javascript
import { Map, List } from "immutable";
import State from "@nathanfaucett/state-immutable";

const state = new State();

let ID = 0;

const todos = state.createStore("todos", {
    list: List()
});

todos.create = text => {
    const id = ID++;

    todos.updateState(state =>
        state.update("list", list =>
            list.push(Map({
                id: id,
                text: text
            }))
        );
    );
};

todos.remove = id => {
    todos.updateState(prev => {
        return prev.update("list", list =>
            list.remove(list.findIndex((todo) => todo.get("id") === id))
        );
    });
};
```
