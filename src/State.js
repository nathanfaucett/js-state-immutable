var EventEmitter = require("@nathanfaucett/event_emitter"),
	arrayForEach = require("@nathanfaucett/array-for_each"),
	immutable = require("immutable"),
	Store = require("./Store");

var Map = immutable.Map,
	fromJS = immutable.fromJS,
	StatePrototype;

module.exports = State;

function State() {
	EventEmitter.call(this, -1);

	this.Store = Store;

	this._stores = Map();
	this._internal = Map();
}
EventEmitter.extend(State);
StatePrototype = State.prototype;

State.Store = StatePrototype.Store = Store;

StatePrototype.forceUpdate = function(name) {
	this.emit("update", name);
	return this;
};

StatePrototype.forceUpdateAll = function() {
	this._stores.forEach(function eachStore(store) {
		store.forceUpdate();
	});
	return this.forceUpdate("*");
};

StatePrototype.state = function() {
	return this._internal;
};

StatePrototype.stateFor = function(name) {
	return this.state().get(name, Map());
};

StatePrototype.setStateFor = function(name, partialState) {
	this.store(name).setState(partialState);
	return this;
};

StatePrototype.updateStateFor = function(name, updateFn) {
	this.store(name).updateState(updateFn);
	return this;
};

StatePrototype.replaceStateFor = function(name, nextState) {
	this.store(name).replaceState(nextState);
	return this;
};

StatePrototype.unsafeSetState = function(unsafeState, emit) {
	var prevInternal = this._internal,
		stores = this._stores,
		emitArray = [];

	unsafeState = fromJS(Object(unsafeState));
	emit = emit !== false;

	this._internal = prevInternal.merge(
		unsafeState.map(function mapState(nextState, name) {
			var prevState = unsafeState.get(name);

			if (emit) {
				emitArray.push([stores.get(name), prevState, nextState]);
			}

			return nextState;
		})
	);

	if (emit) {
		arrayForEach(emitArray, function eachState(args) {
			var store = args[0],
				prevState = args[1],
				nextState = args[2];

			store.emit("unsafeSetState", prevState, nextState);
		});
		this.emit("unsafeSetState", prevInternal, this._internal);
	}

	return this;
};

StatePrototype.unsafeSetStateFor = function(name, unsafeState, emit) {
	var store = this.store(name),
		prevState = this.stateFor(name),
		nextState = fromJS(Object(unsafeState)),
		prevInternal = this._internal;

	this._internal = this._internal.set(name, nextState);

	if (emit !== false) {
		store.emit("unsafeSetStateFor", prevState, nextState);
		store.emit("unsafeSetState", prevState, nextState);

		this.emit("unsafeSetState", prevInternal, this._internal);
	}

	return this;
};

StatePrototype.store = function(name) {
	return this._stores.get(name);
};

StatePrototype.stores = function() {
	return this._stores;
};

StatePrototype.createStore = function(name, initialState) {
	if (this._stores.get(name)) {
		throw new Error("State already has store with name " + name);
	}
	var store = new this.Store(name, this);

	this._stores = this._stores.set(name, store);
	this._internal = this._internal.set(
		name,
		fromJS(Object(initialState || {}))
	);

	this.emit("addStore", store);

	return store;
};

StatePrototype.removeStore = function(name) {
	var store = this._stores.get(name);

	if (!store) {
		throw new Error("State does not have store " + name);
	}

	this._stores = this._stores.remove(name);
	this._internal = this._internal.remove(name);

	this.emit("removeStore", store);

	return this.forceUpdate();
};

StatePrototype.toJSON = function() {
	return this._internal.toJSON();
};

StatePrototype.toJS = function() {
	return this._internal.toJS();
};
