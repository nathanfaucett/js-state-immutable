var EventEmitter = require("@nathanfaucett/event_emitter"),
    immutable = require("immutable");

var fromJS = immutable.fromJS,
    StorePrototype;

module.exports = Store;

function Store(name, state) {
    EventEmitter.call(this, -1);

    this._name = name;
    this._state = state;
}
EventEmitter.extend(Store);
StorePrototype = Store.prototype;

StorePrototype.forceUpdate = function() {
    this.emit("update");
    return this;
};

StorePrototype.name = function() {
    return this._name;
};

StorePrototype.state = function() {
    return this._state.stateFor(this._name);
};

StorePrototype.unsafeSetState = function(nextState, emit) {
    return this._state.unsafeSetStateFor(this._name, nextState, emit);
};

StorePrototype.setState = function(partialState) {
    return this.replaceState(this.state().merge(fromJS(Object(partialState))));
};

StorePrototype.replaceState = function(nextStoreState) {
    this._state._internal = this._state._internal.set(
        this._name,
        fromJS(Object(nextStoreState))
    );

    this.forceUpdate();
    this._state.forceUpdate(this._name);

    return this;
};

StorePrototype.updateState = function(updateFn) {
    return this.replaceState(updateFn(this.state()));
};

StorePrototype.toJSON = function() {
    return this.state().toJSON();
};

StorePrototype.toJS = function() {
    return this.state().toJS();
};
